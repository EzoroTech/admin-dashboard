﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Diagnostics;
using EllokartAdmin.Models;
using EllokartAdmin.Models.Specification;
using System.Net.Http;
using System.Net.Http.Headers;


namespace EllokartAdmin.Controllers


{
    public class ProductSpecificationController : Controller
    {

        ForDb db = new ForDb();
        public static string Baseurl = ForDb.BaseURL;

        private string ProductSpecificationGetUrl = Baseurl + "ProductSpecification/GetAllProductSpecification";
        private string ProductSpecificationOneUrl = Baseurl + "ProductSpecification/GetOneProductSpecification";
        private string ProductSpecAddUrl = Baseurl + "ProductSpecification/ProductSpecification";
        private string ProductSpecDeleteUrl = Baseurl + "ProductSpecification/DeleteProductSpecification";
        private string ProductSpecUpdateUrl = Baseurl + "ProductSpecification/UpdateProductSpecification";

        private string ProductCategListUrl = Baseurl + "ProductCategory/GetAllProductCategory";
        private string SetPdtCategSpec = Baseurl + "ProductSpecification/AddSpecificationAttribute";

        APIResult oResult = null;

        // GET: ProductSpecification
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ProductSpecification()
        {

            return View();
        }



        public ActionResult CreateProductSpec(ProductSpecModel _addPdtSpec)
        {

            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(ProductSpecAddUrl, _addPdtSpec).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }
            return RedirectToAction("ProductSpecList", "ProductSpecification");


        }
        public ActionResult ProductSpecList(ProductSpecGetModel _PdtspecModel)
        {

            string sUrl = ProductSpecificationGetUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _PdtspecModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();

        }

        public ActionResult EditProductSpec(int Id)
        {
            string sUrl = ProductSpecUpdateUrl;
            string mUrl = ProductSpecificationOneUrl;
            ProductSpecModel lmodel = new ProductSpecModel();
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            ProductSpecGetModel oModel = new ProductSpecGetModel()

            {
                ProductSpecId = Id

            };

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(mUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;

            var input = data.sResult;

            ProductSpecModel prdtSpecOut = new ProductSpecModel();


            ViewBag.message = input;
            return View();

        }

        public ActionResult DeletePdtSpec(int Id)
        {
            ProductSpecGetModel _input = new ProductSpecGetModel();
            _input.ProductSpecId = Id;



            string sUrl = ProductSpecDeleteUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return RedirectToAction("ProductSpecList", "ProductSpecification");
        }



        public ActionResult ProductSpecUpdate(int ProductSpecId, string SpecificationName, string description, string Status)
        {
            bool Isactive = false;
          
            if (Status == "on")
            {
                Isactive = true;
            }

            ProductSpecUpdateModel oModel = new ProductSpecUpdateModel()

            {
                ProductSpecificationId = ProductSpecId,
                specificationName = SpecificationName,
                Description = description,
                IsActive = Isactive

                // Flag = 1
            };

        
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(ProductSpecUpdateUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }
         

            return RedirectToAction("ProductSpecList", "ProductSpecification");

        }



        public ActionResult ProductCatSpec(ProductCategModel _pdtCateg)
        {

            //fill dropdown
            string getAllSpecList = ProductSpecificationGetUrl;
            oResult = new APIResult();
            HttpClient client1 = new HttpClient();
            string token1 = string.Empty;
            client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token1);
            HttpResponseMessage response1 = client1.PostAsJsonAsync(getAllSpecList, _pdtCateg).Result;
            response1.EnsureSuccessStatusCode();
            var data1 = response1.Content.ReadAsAsync<APIResult>().Result;
            var input1 = data1.sResult;
            ViewBag.specList = input1;





            string sUrl = ProductCategListUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _pdtCateg).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {

                ViewBag.message = data.sResult;

            }

          return View();
        }
        public ActionResult CreateProductCatSpec(List<Specification> chk)
        {
            Specification _categspec = new Specification();

            var ts = new Specification();
            foreach (var item in chk)
            {
                //ts.Items += item + "/";
            }
          //  ts.Items = ts.Items.Substring(0, ts.Items.Length - 1);

            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(SetPdtCategSpec, _categspec).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }
          
            return RedirectToAction("ProductCatSpecList", "ProductSpecification");


        }
        public ActionResult ProductCatSpecList(ProductSpecGetModel _PdtModel)
        {

            string sUrl = ProductCategListUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _PdtModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();

        }

      

    }
}