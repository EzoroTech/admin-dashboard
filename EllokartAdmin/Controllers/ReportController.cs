﻿using EllokartAdmin.Models;
using EllokartAdmin.Models.CustomerSupport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml;

namespace EllokartAdmin.Controllers
{
    public class ReportController : Controller
    {
        ForDb db = new ForDb();
        public static string Baseurl = ForDb.BaseURL;
        string VerifiedBusinesses = Baseurl + "Verification/GetAllVerifiedBusinesses";
        string UpgragedSeller = Baseurl + "Verification/GetAllUpgradedSeller";
        string searchdata = Baseurl + "Verification/GetOneBusiness";
        string VerificationDataUrl = Baseurl + "Verification/GetAllBusinesses";

        APIResult oResult = null;
        // GET: Report
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult downloadverifiedsellers(BusinessGetModel _input)
        {
            string sUrl = VerifiedBusinesses;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }


            ExcelPackage Ep = new ExcelPackage();

            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");

            Sheet.Cells["A1"].Value = "REG DATE";
            Sheet.Cells["B1"].Value = "VERIFIED DATE";
            Sheet.Cells["C1"].Value = "CLIENT NAME";
            Sheet.Cells["D1"].Value = "BUSSINESS NAME";
            Sheet.Cells["E1"].Value = "EMAIL ID";
            Sheet.Cells["F1"].Value = "NUMBER";
            Sheet.Cells["G1"].Value = "LOCATION";
            Sheet.Cells["H1"].Value = "ADDRESSLINE 1";
            Sheet.Cells["I1"].Value = "ADDRESSLINE 2";
            Sheet.Cells["J1"].Value = "PINCODE";
            Sheet.Cells["K1"].Value = "GST NUMBER";
            Sheet.Cells["L1"].Value = "FRANCHISE CODE";
            Sheet.Cells["M1"].Value = "FRANCHISE NAME";
            Sheet.Cells["N1"].Value = "PLAN";
            Sheet.Cells["O1"].Value = "COMMENTS";
            Sheet.Cells["P1"].Value = "LATTITUDE";
            Sheet.Cells["Q1"].Value = "LONGITUDE";

            int row = 2;
            foreach (var item in ViewBag.message)
            {
                var x = item["Type"];
                Sheet.Cells[string.Format("A{0}", row)].Value = item["CreatedOn"].ToString();
                Sheet.Cells[string.Format("B{0}", row)].Value = item["VerifiedDate"].ToString();
                Sheet.Cells[string.Format("C{0}", row)].Value = item["ContactPerson"].ToString();
                Sheet.Cells[string.Format("D{0}", row)].Value = item["DisplayName"].ToString();
                Sheet.Cells[string.Format("E{0}", row)].Value = item["EmailID"].ToString();
                Sheet.Cells[string.Format("F{0}", row)].Value = item["MobileNo"].ToString();
                Sheet.Cells[string.Format("G{0}", row)].Value = item["LocationName"].ToString();
                Sheet.Cells[string.Format("H{0}", row)].Value = item["Addresslineone"].ToString();
                Sheet.Cells[string.Format("I{0}", row)].Value = item["Addresslinetwo"].ToString();
                Sheet.Cells[string.Format("J{0}", row)].Value = item["Pincode"].ToString();
                Sheet.Cells[string.Format("K{0}", row)].Value = item["GSTNO"].ToString();
                Sheet.Cells[string.Format("L{0}", row)].Value = item["FranchiseName"].ToString();
                Sheet.Cells[string.Format("M{0}", row)].Value = item["FranchiseName"].ToString();
                Sheet.Cells[string.Format("N{0}", row)].Value = item["MembershipPlan"].ToString();
                Sheet.Cells[string.Format("O{0}", row)].Value = item["Comments"].ToString();
                Sheet.Cells[string.Format("P{0}", row)].Value = item["Lattittude"].ToString();
                Sheet.Cells[string.Format("Q{0}", row)].Value = item["Longittude"].ToString();

                row++;
            }


            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "SellerDetails.xlsx");
            Response.BinaryWrite(Ep.GetAsByteArray());
            Response.End();
            return View();
        }

        public ActionResult downloadRegisterdsellers(BusinessGetModel _input)
        {
            string sUrl = VerificationDataUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }


            ExcelPackage Ep = new ExcelPackage();

            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");

            Sheet.Cells["A1"].Value = "REG DATE";
            Sheet.Cells["B1"].Value = "VERIFIED DATE";
            Sheet.Cells["C1"].Value = "CLIENT NAME";
            Sheet.Cells["D1"].Value = "BUSSINESS NAME";
            Sheet.Cells["E1"].Value = "EMAIL ID";
            Sheet.Cells["F1"].Value = "NUMBER";
            Sheet.Cells["G1"].Value = "LOCATION";
            Sheet.Cells["H1"].Value = "ADDRESSLINE 1";
            Sheet.Cells["I1"].Value = "ADDRESSLINE 2";
            Sheet.Cells["J1"].Value = "PINCODE";
            Sheet.Cells["K1"].Value = "GST NUMBER";
            Sheet.Cells["L1"].Value = "FRANCHISE CODE";
            Sheet.Cells["M1"].Value = "FRANCHISE NAME";
            Sheet.Cells["N1"].Value = "PLAN";
            Sheet.Cells["O1"].Value = "COMMENTS";
            Sheet.Cells["P1"].Value = "LATTITUDE";
            Sheet.Cells["Q1"].Value = "LONGITUDE";

            int row = 2;
            foreach (var item in ViewBag.message)
            {
                var x = item["Type"];
                Sheet.Cells[string.Format("A{0}", row)].Value = item["CreatedOn"].ToString();
                Sheet.Cells[string.Format("B{0}", row)].Value = item["VerifiedDate"].ToString();
                Sheet.Cells[string.Format("C{0}", row)].Value = item["ContactPerson"].ToString();
                Sheet.Cells[string.Format("D{0}", row)].Value = item["DisplayName"].ToString();
                Sheet.Cells[string.Format("E{0}", row)].Value = item["EmailID"].ToString();
                Sheet.Cells[string.Format("F{0}", row)].Value = item["MobileNo"].ToString();
                Sheet.Cells[string.Format("G{0}", row)].Value = item["LocationName"].ToString();
                Sheet.Cells[string.Format("H{0}", row)].Value = item["Addresslineone"].ToString();
                Sheet.Cells[string.Format("I{0}", row)].Value = item["Addresslinetwo"].ToString();
                Sheet.Cells[string.Format("J{0}", row)].Value = item["Pincode"].ToString();
                Sheet.Cells[string.Format("K{0}", row)].Value = item["GSTNO"].ToString();
                Sheet.Cells[string.Format("L{0}", row)].Value = item["FranchiseName"].ToString();
                Sheet.Cells[string.Format("M{0}", row)].Value = item["FranchiseName"].ToString();
                Sheet.Cells[string.Format("N{0}", row)].Value = item["MembershipPlan"].ToString();
                Sheet.Cells[string.Format("O{0}", row)].Value = item["Comments"].ToString();
                Sheet.Cells[string.Format("P{0}", row)].Value = item["Lattittude"].ToString();
                Sheet.Cells[string.Format("Q{0}", row)].Value = item["Longittude"].ToString();

                row++;
            }


            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "SellerDetails.xlsx");
            Response.BinaryWrite(Ep.GetAsByteArray());
            Response.End();
            return View();
        }

        public ActionResult downloadUpgradededsellers(BusinessGetModel _input)
        {
            string sUrl = UpgragedSeller;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }


            ExcelPackage Ep = new ExcelPackage();

            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
            
            Sheet.Cells["A1"].Value = "REG DATE";
            Sheet.Cells["B1"].Value = "VERIFIED DATE";
            Sheet.Cells["C1"].Value = "CLIENT NAME";
            Sheet.Cells["D1"].Value = "BUSSINESS NAME";
            Sheet.Cells["E1"].Value = "EMAIL ID";
            Sheet.Cells["F1"].Value = "NUMBER";
            Sheet.Cells["G1"].Value = "LOCATION";
            Sheet.Cells["H1"].Value = "ADDRESSLINE 1";
            Sheet.Cells["I1"].Value = "ADDRESSLINE 2";
            Sheet.Cells["J1"].Value = "PINCODE";
            Sheet.Cells["K1"].Value = "GST NUMBER";
            Sheet.Cells["L1"].Value = "FRANCHISE CODE";
            Sheet.Cells["M1"].Value = "FRANCHISE NAME";
            Sheet.Cells["N1"].Value = "PLAN";
            Sheet.Cells["O1"].Value = "COMMENTS";
            Sheet.Cells["P1"].Value = "LATTITUDE";
            Sheet.Cells["Q1"].Value = "LONGITUDE";


            int row = 2;
            foreach (var item in ViewBag.message)
            {
                var x = item["Type"];

               
                Sheet.Cells[string.Format("A{0}", row)].Value = item["CreatedOn"].ToString();
                Sheet.Cells[string.Format("B{0}", row)].Value = item["VerifiedDate"].ToString();
                Sheet.Cells[string.Format("C{0}", row)].Value = item["ContactPerson"].ToString();
                Sheet.Cells[string.Format("D{0}", row)].Value = item["DisplayName"].ToString();
                Sheet.Cells[string.Format("E{0}", row)].Value = item["EmailID"].ToString();
                Sheet.Cells[string.Format("F{0}", row)].Value = item["MobileNo"].ToString();
                Sheet.Cells[string.Format("G{0}", row)].Value = item["LocationName"].ToString();
                Sheet.Cells[string.Format("H{0}", row)].Value = item["Addresslineone"].ToString();
                Sheet.Cells[string.Format("I{0}", row)].Value = item["Addresslinetwo"].ToString();
                Sheet.Cells[string.Format("J{0}", row)].Value = item["Pincode"].ToString();
                Sheet.Cells[string.Format("K{0}", row)].Value = item["GSTNO"].ToString();
                Sheet.Cells[string.Format("L{0}", row)].Value = item["FranchiseName"].ToString();
                Sheet.Cells[string.Format("M{0}", row)].Value = item["FranchiseName"].ToString();
                Sheet.Cells[string.Format("N{0}", row)].Value = item["MembershipPlan"].ToString();
                Sheet.Cells[string.Format("O{0}", row)].Value = item["Comments"].ToString();
                Sheet.Cells[string.Format("P{0}", row)].Value = item["Lattittude"].ToString();
                Sheet.Cells[string.Format("Q{0}", row)].Value = item["Longittude"].ToString();
                row++;
            }


            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "SellerDetails.xlsx");
            Response.BinaryWrite(Ep.GetAsByteArray());
            Response.End();
            return View();
        }
        public ActionResult downloadsearchdata(int Id)
        {
            

            BusinessGetModel _input = new BusinessGetModel();
            _input.UserId = Id;
            string sUrl = searchdata;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
               response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;


            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }


            ExcelPackage Ep = new ExcelPackage();

            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");

            Sheet.Cells["A1"].Value = "REG DATE";
            Sheet.Cells["B1"].Value = "VERIFIED DATE";
        
            int row = 2;
            foreach (var item in ViewBag.message)
            {
                var x = item["Type"];
                Sheet.Cells[string.Format("A{0}", row)].Value = item["CreatedOn"].ToString();
                Sheet.Cells[string.Format("B{0}", row)].Value = item["VerifiedDate"].ToString();
              

                row++;
            }


            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "SellerDetails.xlsx");
            Response.BinaryWrite(Ep.GetAsByteArray());
            Response.End();
            return View();
        }

    }
}