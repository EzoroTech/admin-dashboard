﻿using EllokartAdmin.Models;
using EllokartAdmin.Models.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace EllokartAdmin.Controllers
{
    public class PaymentController : Controller
    {
        // GET: Payment
        ForDb db = new ForDb();
        public static string Baseurl = ForDb.BaseURL;
        private string GetMembershipPlan = Baseurl + "MembershipPlan/AllMembershipPlans";
        private string GeneratePaymentCodeUrl = Baseurl + "Franchise/GeneratePaymentCode";
        APIResult oResult = null;
        // GET: Payment
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GeneratePaymentCode()
        {
            //String Id=null;
            string sUrl = GetMembershipPlan;
            Payment _payment = new Payment();
            //_payment.MobileNumber = Id;

            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _payment).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            var details = data.sResult;
            if (data.sMessage == "Successfully collected all membership plans ")
            {

                ViewBag.message = data.sResult;
            }

            return View();
        }
        public ActionResult PaymentCodeGenearate(long MembershipPlanId, string MobileNumber)
        {
            //String Id=null;
            string sUrl = GeneratePaymentCodeUrl;
            PaymentCode _payment = new PaymentCode();
            _payment.MobileNo = MobileNumber;
            _payment.PlanId = MembershipPlanId;

            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _payment).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            var details = data.sResult;
            if (data.sMessage == "Success")
            {               
                ViewBag.message = data.sResult;
                return View();
            }
            else
            {
                ViewBag.message = data.sMessage;
                return View();
            }
            
            
        }
    }
}