﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Diagnostics;
using EllokartAdmin.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using EllokartAdmin.Models.MmbershipPlan;

namespace EllokartAdmin.Controllers
{
    public class MembershipPlanController : Controller
    {
        // GET: MembershipPlan
        public ActionResult Index()
        {
            return View();
        }

        ForDb db = new ForDb();
        public static string Baseurl = ForDb.BaseURL;
        private string MembershipPlanListUrl = Baseurl + "MembershipPlan/GetAllMembershipPlan";
        private string OneMembershipPlanUrl = Baseurl + "MembershipPlan/GetOneMembershipPlan";
        private string MembershipPlanUrl = Baseurl + "MembershipPlan/CreateMembershipPlan";
        private string UpdateMembershipPlanUrl = Baseurl + "MembershipPlan/UpdateMembershipPlan";
        private string DeleteMembershipPlanUrl = Baseurl + "MembershipPlan/DeleteAMembershipPlan";

        APIResult oResult = null;

        public ActionResult MembershipPlan()
        {
            return View();

        }
        public ActionResult MembershipPlanList(membershipGetModel _membershipPlan)
        {

            string sUrl = MembershipPlanListUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _membershipPlan).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();

        }
       public ActionResult CreateMembershipPlan(membershipOutputModel _membershipOutput)
        {
           
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            membershipOutputModel oModel = new membershipOutputModel();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(MembershipPlanUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }
           return RedirectToAction("MembershipPlanList", "MembershipPlan");
           
        }
        public ActionResult EditMembershipPlan(int Id)
        {
            string sUrl = UpdateMembershipPlanUrl;
            string mUrl = OneMembershipPlanUrl;
            membershipOutputModel lmodel = new membershipOutputModel();
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            membershipGetModel oModel = new membershipGetModel()

            {
                MembershipPlanId = Id

            };

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(mUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;

            var input = data.sResult;

            membershipOutputModel moduleOut = new membershipOutputModel();


            ViewBag.message = input;
            return View();

        }
        public ActionResult MembershipPlanUpdate(int MembershipPlanid, string Membershipplan, int price,int validityInMonth, string description, string status,int Taxid)
        {
            bool Isactive = false;

            if (status == "on")
            {
                Isactive = true;
            }
            membershipUpdateModel mModel = new membershipUpdateModel()

            {
                MembershipPlanId = MembershipPlanid,
                MembershipPlan= Membershipplan,
                Price= price,
                ValidityInMonths=validityInMonth,
                Description=description,
                IsActive= Isactive,
                TaxId=Taxid






                // Flag = 1
            };
            string sUrl = UpdateMembershipPlanUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, mModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }

            return RedirectToAction("MembershipPlanList", "MembershipPlan");

        }

        public ActionResult DeleteMembershipPlan(int Id)
        {
            membershipGetModel _input = new membershipGetModel();
            _input.MembershipPlanId = Id;



            string sUrl = DeleteMembershipPlanUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return RedirectToAction("MembershipPlanList", "MembershipPlan");
        }


    }
}