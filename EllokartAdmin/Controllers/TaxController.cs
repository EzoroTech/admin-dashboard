﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using EllokartAdmin.Models.Tax;
using EllokartAdmin.Models;




namespace EllokartAdmin.Controllers
{
    public class TaxController : Controller
    {
        ForDb db = new ForDb();
        public static string Baseurl = ForDb.BaseURL;
        private string TaxListUrl = Baseurl + "Tax/GetAllTaxes";
        private string AddTaxUrl = Baseurl + "Tax/CreateTax";
        private string TaxDeleteUrl = Baseurl + "Tax/DeleteATax";
        private string UpdateTaxUrl = Baseurl + "Tax/UpdateTax";
        private string OneTaxUrl = Baseurl + "Tax/GetOneTax";
        APIResult oResult = null;


        // GET: Tax
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Tax()
        {
            return View();
        }

        public ActionResult TaxList(GetTaxModel _Tax)
        {      

            string sUrl = TaxListUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty     ;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _Tax).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();

        }

        public ActionResult CreateTax(TaxInput _Tax)
        {

           

            string sUrl = AddTaxUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            TaxInput oModel = new TaxInput();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _Tax).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Successfully added Tax")
            {

            }

            return RedirectToAction("TaxList", "Tax");

        }

        public ActionResult EditTax(int Id)
        {
            string sUrl = UpdateTaxUrl;
            string mUrl = OneTaxUrl;
            TaxInput lmodel = new TaxInput();
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            GetTaxModel oModel = new GetTaxModel()

            {
                TaxId = Id

            };

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(mUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;

            var input = data.sResult;

            TaxInput TaxOut = new TaxInput();


            ViewBag.message = input;
            return View();

        }

        public ActionResult TaxUpdate(int TaxId, string TaxName, string TaxPercentage, string Status)
        {
            bool IsActive = false;
          
            if (Status == "on")
            {
                IsActive = true;
            }

            TaxUpdateModel oModel = new TaxUpdateModel()

            {
                TaxId = TaxId,
                TaxName = TaxName,
                taxPercentage = TaxPercentage,
                IsActive = IsActive

                // Flag = 1
            };


            string sUrl = UpdateTaxUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            // TaxOutputModel oModel = new TaxOutputModel();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }
            return RedirectToAction("TaxList", "Tax");

        }

        public ActionResult DeleteTax(int Id)
        {
            GetTaxModel _input = new GetTaxModel();
            _input.TaxId = Id;



            string sUrl = TaxDeleteUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return RedirectToAction("TaxList", "Tax");
        }
    }
}