﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Diagnostics;
using EllokartAdmin.Models.Business;
using EllokartAdmin.Models;
using System.Net.Http;
using System.Net.Http.Headers;


namespace EllokartAdmin.Controllers
{
    public class BusinessController : Controller
    {
        ForDb db = new ForDb();
        public static string Baseurl = ForDb.BaseURL;
        private string BusinessTypeListUrl = Baseurl + "Business/GetAllBusinessType";
        private string BusinessTypeAdd = Baseurl + "Business/CreateBusinessType";
        private string BusinessTypeDelete = Baseurl + "Business/DeleteBusinessType";
        private string BusinessTypeUpdateUrl = Baseurl + "Business/UpdateBusinessType";
        private string OneBusinessType = Baseurl + "Business/GetOneBusinessType";

        private string BusinessCategListUrl = Baseurl + "BusinessCategory/GetAllBusinessCategoryAdmin";
        private string BusinessCategAddUrl = Baseurl + "BusinessCategory/CreateBusinessCategory";
        private string BusinessCategDelete = Baseurl + "BusinessCategory/DeleteBusinessCategory";
        private string BusinessCategoryUpdateUrl = Baseurl + "BusinessCategory/UpdateBusinessCategory";
        private string OneBusinessCateg = Baseurl + "BusinessCategory/GetOneBusinessCateg";



        APIResult oResult = null;



        // GET: Business
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult BusinessType()
        {

            return View();
        }

        public ActionResult BusinessTypeList(GetBusinessType _BusinessType)
        {
            string sUrl = BusinessTypeListUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _BusinessType).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();

        }

        public ActionResult CreateBusinesssType(BusinessTypeModel _bType)
        {

            string sUrl = BusinessTypeAdd;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            BusinessModel bModel = new BusinessModel();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _bType).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }

            return RedirectToAction("BusinessTypeList", "Business"); 
        }

        public ActionResult EditBusinessType(int Id)
        {
            string sUrl = BusinessTypeUpdateUrl;
            string mUrl = OneBusinessType;
            BusinessTypeModel lmodel = new BusinessTypeModel();
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            GetBusinessType oModel = new GetBusinessType()

            {
                bussinessTypeId = Id

            };

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(mUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;

            var input = data.sResult;

            BusinessTypeModel moduleOut = new BusinessTypeModel();


            ViewBag.message = input;
            return View();

        }

        public ActionResult BusinessTypeUpdate(int BussinessTypeId, string BussinessTypeName, string bussinessTypeDecr, string Status)
        {
            bool isActive = false;

            if (Status == "on")
            {
                isActive = true;
            }
            BusinessTypeUpdateModel oModel = new BusinessTypeUpdateModel()

            {
                bussinessTypeId = BussinessTypeId,
                bussinessTypeName = BussinessTypeName,
                bussinessTypeDecr = bussinessTypeDecr,
                isActive = isActive

                // Flag = 1
            };

            string sUrl = BusinessTypeUpdateUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            //  BusinessModel oModel = new BusinessModel();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }

            return RedirectToAction("BusinessTypeList", "Business");

        }


        public ActionResult DeleteBusinessType(int Id)
        {
            GetBusinessType _input = new GetBusinessType();
            _input.bussinessTypeId = Id;



            string sUrl = BusinessTypeDelete;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return RedirectToAction("BusinessTypeList", "Business");
        }


        public ActionResult BusinessCategory(GetBusinessType _BusinessType)
        {
            string sUrl = BusinessTypeListUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _BusinessType).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();
        }
        public ActionResult BusinessCategoryList(GetBusinessCategory _BusinessCateg)
        {
            string sUrl = BusinessCategListUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _BusinessCateg).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();

        }
        public ActionResult CreateBusinesssCateg(int BussinessTypeId, string bussinessCategoryName, string Description, string status)
        {



            BusinessModel _input = new BusinessModel()
            {
                bussinessTypeId = BussinessTypeId,
                bussinessCategoryName = bussinessCategoryName,
                bussinessCategoryDecr = Description,
                isActive = true
            };

            string sUrl = BusinessCategAddUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            BusinessModel bModel = new BusinessModel();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }

            return RedirectToAction("BusinessCategoryList", "Business");


        }

        public ActionResult EditBusinessCategory(int Id)
        {

            //fill dropdown
            string businessTypeUrl = BusinessTypeListUrl;
            oResult = new APIResult();
            HttpClient client1 = new HttpClient();
            string token1 = string.Empty;
            client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token1);
            HttpResponseMessage response1 = client1.PostAsJsonAsync(businessTypeUrl, Id).Result;
            response1.EnsureSuccessStatusCode();
            var data1 = response1.Content.ReadAsAsync<APIResult>().Result;
            var input1 = data1.sResult;
            ViewBag.dropdown = input1;




            string sUrl = BusinessTypeUpdateUrl;
            string mUrl = OneBusinessCateg;
            BusinessModel lmodel = new BusinessModel();
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            GetBusinessCategory oModel = new GetBusinessCategory()

            {
                bussinessCategoryId = Id

            };

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(mUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;

            var input = data.sResult;

            BusinessModel moduleOut = new BusinessModel();


            ViewBag.message = input;
            return View();

        }

        public ActionResult BusinessCategoryUpdate(int BussinessCategoryId, int BusinessTypeid, string BusinessName, string Description, string status)
        {
            bool Isactive = false;
          
            if (status == "on")
            {
                Isactive = true;
            }

            BusinessCategUpdateModel oModel = new BusinessCategUpdateModel()

            {

                bussinessCategoryId = BussinessCategoryId,
                bussinessTypeId = BusinessTypeid,
                bussinessCategoryName = BusinessName,
                bussinessCategoryDecr = Description,
                isActive = Isactive

                // Flag = 1
            };

            string sUrl = BusinessCategoryUpdateUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            BusinessModel lModel = new BusinessModel();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }
            return RedirectToAction("BusinessCategoryList", "Business");

        }

        public ActionResult DeleteBusinessCategory(int Id)
        {
            GetBusinessCategory _input = new GetBusinessCategory();
            _input.bussinessCategoryId = Id;



            string sUrl = BusinessCategDelete;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return RedirectToAction("BusinessCategoryList", "Business");
        }
    }
}