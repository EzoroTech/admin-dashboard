﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Diagnostics;
using EllokartAdmin.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using EllokartAdmin.Models.Login;

namespace EllokartAdmin.Controllers
{
    public class LoginController : Controller
    {
        ForDb db = new ForDb();
        public static string Baseurl = ForDb.BaseURL;
        private string LoginUrl = Baseurl + "Login/AdminLogin";

        APIResult oResult = null;


        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();

        }


        public ActionResult VerifyLogin(string userid, string password, int UserTypeId)
        {
            LoginModel _input = new LoginModel();
            _input.UserName = userid;
            _input.Password = password;
            _input.UserType = UserTypeId;
            string sUrl = LoginUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            if (_input.UserType == 1)
            {
                return RedirectToAction("VerifyAdminLogin", "LogIn", _input);
            }
            else if (_input.UserType == 2)
            {
                return RedirectToAction("VerifyCustomerLogin", "LogIn", _input);
            }
            else
            {
                return RedirectToAction("Index", "Staff", _input);
            }
        }

        public ActionResult VerifyAdminLogin(LoginModel _input)
        {

            string sUrl = LoginUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;

            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult VerifyCustomerLogin(LoginModel _input)
        {
            string sUrl = LoginUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;

            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Login", "Login", _input);
            }
        }
    }
}