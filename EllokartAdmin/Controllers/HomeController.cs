﻿using System;
using System.Web;
using System.Linq;
using System.Web.Mvc;
using System.Net.Http;
using EllokartAdmin.Models;
using System.Net.Http.Headers;
using System.Collections.Generic;
using EllokartAdmin.Models.CustomerSupport;
using EllokartAdmin.Models.ProductCategory;



namespace EllokartAdmin.Controllers
{
    public class HomeController : Controller
    {

        ForDb db = new ForDb();
        public static string Baseurl = ForDb.BaseURL;
        string VerificationDataUrl = Baseurl + "Verification/GetAllBusinesses";
        string VerificationUrl = Baseurl + "Verification/GetOneBusiness";
        string VerifyingUrl = Baseurl + "Verification/BusinessRejection";

        string VerifiedBusinesses = Baseurl + "Verification/GetAllVerifiedBusinesses";
        string UpdateVerifiedBusiness = Baseurl + "Verification/UpdateVerifiedBusiness";
        string OneVerifiedBusiness = Baseurl + "Verification/GetOneVerifiedSeller";

        string FranchiseListUrl = Baseurl + "Franchise/GetAllFranchises";
        string FranchiseSellerCodeUrl = Baseurl + "Franchise/GeneratePaymentCode";
        string UpdateEditedBusiness = Baseurl + "Verification/UpdateVerifiedBusiness";

        string BusinessUpgradeList = Baseurl + "Verification/GetBusinessToUpgrade";
        string UpgradeUrl = Baseurl + "Verification/UpdateStatusOfBusiness";

        private string BrandAddUrl = Baseurl + "ProductBrand/CreateProductBrand";
        private string ProductListUrl = Baseurl + "ProductBrand/GetAllProductBrand";
        private string getoneproductbrand = Baseurl + "ProductBrand/GetProductBrandById";
        private string updateproductbrand = Baseurl + "ProductBrand/UpdateProductBrand";
        private string deletebrand = Baseurl + "ProductBrand/DeleteProductBrand";
        private string getallparentcategory = Baseurl + "ProductCategory/GetParentCategory";
        private string getallproductcategory = Baseurl + "ProductCategory/GetAllProductCategory";

        string searchbyMobnumber = Baseurl + "Search/GetSellerWithMobileNo";
        string UpgragedSeller = Baseurl + "Verification/GetAllUpgradedSeller";
        string searchbyDate = Baseurl + "Search/GetSellerWithCreatedDate";

        string SaveCustomerDataUrl = Baseurl + "Home/AddCustomerSupportData";

        APIResult oResult = null;

        public ActionResult Index()
        {
            return View();
        }

       public ActionResult ReferenceCode()
        {
            int id = 0;
            string GetFranchises = FranchiseListUrl;
            oResult = new APIResult();     
            HttpClient client1 = new HttpClient();
            string token1 = string.Empty;
            client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token1);
            HttpResponseMessage response1 = client1.PostAsJsonAsync(GetFranchises, id).Result;
            response1.EnsureSuccessStatusCode();
            var data1 = response1.Content.ReadAsAsync<APIResult>().Result;
            var input1 = data1.sResult;
            ViewBag.dropdown = input1;

            return View();

        }

        public ActionResult GenerateSellerCode(string FranchiseCode, int PlanId)
        {

            CustomerSuppoerMdl code = new CustomerSuppoerMdl();
            code.createdBy = FranchiseCode;
            code.PlanId = PlanId;

            string sUrl = FranchiseSellerCodeUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, code).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }

            return View();
        }

        public ActionResult BusinessVerificationList(BusinessGetModel _VerifyList)
        {

            string sUrl = VerificationDataUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _VerifyList).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();
        }


        public ActionResult BusinessVerification(int Id)
        {
            BusinessGetModel _input = new BusinessGetModel();
            _input.UserId = Id;
            string sUrl = VerificationUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();
        }


        public ActionResult BusinessVerifyingList(int Id, int accept)
        {
            BusinessApprovalModel _input = new BusinessApprovalModel();
            _input.UserBusinessId = Id;
            _input.IsActive = accept;     
            string sUrl = VerifyingUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return RedirectToAction("BusinessVerificationList", "Home");
        }

        public ActionResult VerifiedBusinessList(BusinessGetModel _input)
        {
            string sUrl = VerifiedBusinesses;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();
        }

        public ActionResult EditVerifiedBusinesses(int Id)
        {
            // fill the fields to edit

            string mUrl = VerificationUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            BusinessGetModel oModel = new BusinessGetModel()

            {
                UserId = Id

            };

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(mUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;

            var input = data.sResult;

            verifiedBusinessUpdate moduleOut = new verifiedBusinessUpdate();


            ViewBag.message = input;
            return View();
        }

        public ActionResult UpdateVerifiedBusinesses(int branchId,string Business, string MobileNumber, string description, string LocationName, string MembershipPlan, string BranchName, string ContactPerson, string Addresslineone, string Addresslinetwo, string Pincode,string EmailId)
        {

            verifiedBusinessUpdate oModel = new verifiedBusinessUpdate()

            {
                BranchId = branchId,
                BusinessDisplayName = Business,
                LocationName = LocationName,
                AddressLine1 = Addresslineone,
                AddressLine2 = Addresslinetwo,
                BusinessName = BranchName,
                ContactPerson = ContactPerson,
                Pincode = Pincode,
                EmailId = EmailId,
                Description = description             
            };
            string sUrl = UpdateEditedBusiness;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }

            return RedirectToAction("VerifiedBusinessList", "Home");
       }

        public ActionResult UpgradeBusinessList(BusinessGetModel _input)
        {
            string sUrl = BusinessUpgradeList;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();
        }


        public ActionResult SellerUpgradation(int Id, int BusinessStatus)
        {
            BusinessUpgradeModel _input = new BusinessUpgradeModel();
            {
                _input.UserBusinessId = Id;
                _input.BusinessStatus = BusinessStatus;
            }

            string sUrl = UpgradeUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return RedirectToAction("UpgradeBusinessList", "Home");

        }

        public ActionResult Upgradededsellers(BusinessGetModel _input)
        {
            string sUrl = UpgragedSeller;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();
        }

        public ActionResult ProductBrandList(ProductBrand _module)
        {

            string sUrl = ProductListUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _module).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();

        }

        public ActionResult createBrand(ProductCategory _module)
        {
            string sUrl = getallparentcategory;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _module).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();
        }

        public ActionResult ProductBrand(int ProductCategoryId, string BrandCode, string BrandName, string Description, string ManufactureName)
        {
            string sUrl = BrandAddUrl;
            ProductBrand _pbrand = new ProductBrand();
            _pbrand.ProductCategoryId = ProductCategoryId;
            _pbrand.BrandCode = BrandCode;
            _pbrand.BrandName = BrandName;
            _pbrand.Description = Description;
            _pbrand.ManufactureName = ManufactureName;
            _pbrand.CreatedBy = "Support";

            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _pbrand).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return RedirectToAction("ProductBrandList");
        }

        public ActionResult EditProductBrand(int Id)
        {

            oResult = new APIResult();

            string url = getoneproductbrand;
            ProductbrandbyId _pbrand = new ProductbrandbyId();
            _pbrand.BrandId = Id;

            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(url, _pbrand).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }

            //-- dropdown
            string sUrl = getallproductcategory;
            ProductCategory _module = new ProductCategory();
            oResult = new APIResult();
            HttpClient client1 = new HttpClient();
            string token1 = string.Empty;
            client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token1);
            HttpResponseMessage response1 = client1.PostAsJsonAsync(sUrl, _module).Result;
            response1.EnsureSuccessStatusCode();
            var data1 = response1.Content.ReadAsAsync<APIResult>().Result;
            if (data1.sMessage == "Success")
            {
                ViewBag.dropdown = data1.sResult;
            }

            return View();

        }

        public ActionResult DeleteProductBrand(int Id)
        {
            updatebrand _brand = new updatebrand();
            _brand.BrandId = Id;

            string sUrl = deletebrand;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _brand).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return RedirectToAction("ProductBrandList");
        }

        public ActionResult UpdateProductBrand(long BrandId, int ProductCategoryId, string BrandCode, string BrandName, string Description, string Manufacturename)
        {
            oResult = new APIResult();

            string url = updateproductbrand;
            updatebrand _brand = new updatebrand();
            _brand.BrandId = BrandId;
            _brand.BrandCode = BrandCode;
            _brand.BrandName = BrandName;
            _brand.BrandDescription = Description;
            _brand.ManufactureName = Manufacturename;

            _brand.ProductCategoryId = ProductCategoryId;

            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(url, _brand).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return RedirectToAction("ProductBrandList");
        }

        public ActionResult SearchMobilenumber(string Mobilenumber,int id)
        {
            SearchByNumber _input = new SearchByNumber();
            _input.MobileNo = Mobilenumber;
            string sUrl = searchbyMobnumber;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;

            }
            if (id == 1)
            {
                return PartialView("SearchByNumber");
            }
            else if (id == 2)
            {            
                return PartialView("Businessverifiedsearch");
            }
            else if (id == 3)
            {
                return PartialView("Businessverifiedsearch");
            }
            else
            {
                return View();
            }

        }

        public ActionResult SearchBydate(string Date, int id)
        {
            SearchByDate _input = new SearchByDate();
            _input.CreatedOn = Date;
            string sUrl = searchbyDate;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;

            }
            if (id == 1)
            {
                return PartialView("SearchByNumber");
            }
            else if (id == 2)
            {
                return PartialView("Businessverifiedsearch");
            }
            else if (id == 3)
            {
                return PartialView("Businessverifiedsearch");
            }
            else
            {
                return View();
            }
        }

        public ActionResult SaveCustomerData(int Id, string comment)
        {
            CommentSaveModel _input = new CommentSaveModel();
            _input.UserId = Id;
            _input.Comments = comment;
            string sUrl = SaveCustomerDataUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            //if (data.sMessage == "Success")
            //{
            //    ViewBag.message = data.sResult;
            //}
            return RedirectToAction("BusinessVerificationList", "Home");
        }
    }
}