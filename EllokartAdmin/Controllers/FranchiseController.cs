﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Diagnostics;
using EllokartAdmin.Models;
using EllokartAdmin.Models.State;
using EllokartAdmin.Models.Franchise;
using System.Net.Http;
using System.Net.Http.Headers;
using EllokartAdmin.Models.Login;


namespace EllokartAdmin.Controllers
{
    public class FranchiseController : Controller
    {

        ForDb db = new ForDb();
        public static string Baseurl = ForDb.BaseURL;
        private string FranchiseListUrl = Baseurl + "Franchise/GetAllFranchises";
        private string OneFranchiseUrl = Baseurl + "Franchise/GetOneFranchise";
        private string FranchiseUrl = Baseurl + "Franchise/CreateFranchise";
        private string UpdateFranchise = Baseurl + "Franchise/UpdateFranchise";
        private string DeleteFranchiseUrl = Baseurl + "Franchise/DeleteFranchise";
        private string GetSellerListForFranchiseUrl = Baseurl + "Franchise/SellerDetailsByFranchise";

        private string LoginUrl = Baseurl + "Login/AdminLogin";
        private string GetCountryUrl = Baseurl + "Country/GetCountryList";
        private string GetsateUrl = Baseurl + "State/GetStateList";
        APIResult oResult = null;

        public static string _franchiseCod;



        // GET: Franchise
        public ActionResult Index()
        {
            return View();
        }





        public ActionResult Franchise()
        {

            StateMdl _input = new StateMdl();
            {
                _input.CountryId = 1;
            }
           



            //////fill dropdown for country
            ////string countryurl = GetCountryUrl;
            ////oResult = new APIResult();
            ////HttpClient client1 = new HttpClient();
            ////string token1 = string.Empty;
            ////client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token1);
            ////HttpResponseMessage response1 = client1.PostAsJsonAsync(countryurl, CntryId).Result;
            ////response1.EnsureSuccessStatusCode();
            ////var data1 = response1.Content.ReadAsAsync<APIResult>().Result;
            ////var input1 = data1.sResult;
            ////ViewBag.Message = input1;


            //fill dropdown for state
            string stateurl = GetsateUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(stateurl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            var input = data.sResult;
            ViewBag.dropdown = input;


            return View();
        }

        public ActionResult FranchiseList(FrachiseGetModel _franchise)
        {

            string sUrl = FranchiseListUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _franchise).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();

        }
        public ActionResult CreateFranchise(string franchiseCode,string phoneNo, string EmailId, string FirstName, string LastName, DateTime DOB, string GenderId, string Password, string status)
        {

            //var s = DOB;
            //var t = s.ToString();
            bool Isactive = false;
            if (status == "on")
            {
                Isactive = true;
            }

            FranchiseMdl _input = new FranchiseMdl()
            {
                FranchiseCode=franchiseCode,
                PhoneNo = phoneNo,
                EmailId = EmailId,
                FirstName = FirstName,
                LastName = LastName,
                DOB = DOB,
                Gender = GenderId,
                Password = Password,
                IsActive = Isactive
            };
            string sUrl = FranchiseUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
           // FranchiseOutputModel oModel = new FranchiseOutputModel();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }

            return RedirectToAction("FranchiseList", "Franchise");

        }
        public ActionResult EditFranchise(int Id)


        {
            string sUrl = UpdateFranchise;
            string mUrl = OneFranchiseUrl;
          
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            FrachiseGetModel oModel = new FrachiseGetModel()

            {
                FranchiseId = Id

            };

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(mUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;

            var input = data.sResult;

            FranchiseMdl FranchiseOut = new FranchiseMdl();


            ViewBag.message = input;
            return View();

        }
        public ActionResult FranchiseUpdate(int FranchiseId, string franchiseCode, string PhoneNumber, string EmailId, string FirstName,
                                            string LastName, DateTime DOB, string Gender, string Status, string Password)
        {
            bool Isactive = false;
           
            if (Status == "on")
            {
                Isactive = true;
            }
            FranchiseUpdateModel oModel = new FranchiseUpdateModel()

            {
                FranchiseId = FranchiseId,
                FranchiseCode=franchiseCode,
                PhoneNumber = PhoneNumber,
                EmailId = EmailId,
                FirstName = FirstName,
                LastName = LastName,
                DOB = DOB,
                Gender = Gender,
                IsActive = Isactive,
                Password = Password


            };
            string sUrl = UpdateFranchise;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
           
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }
        
            return RedirectToAction("FranchiseList", "Franchise");

        }


        public ActionResult DeleteFranchise(int Id)
        {
            FrachiseGetModel _input = new FrachiseGetModel();
            _input.FranchiseId = Id;



            string sUrl = DeleteFranchiseUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return RedirectToAction("FranchiseList", "Franchise");
        }

        public ActionResult FranchiseSellerList()

        {
            //var _FranchiseCode = HttpContext.Session.GetString("FranchiseCode");

            FranchiseSeller _frnchsSeller = new FranchiseSeller();
            _frnchsSeller.FranchiseCode = _franchiseCod;

            string sUrl = GetSellerListForFranchiseUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _frnchsSeller).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {

                ViewBag.message = data.sResult;
            }

            return View();
        }
        public ActionResult VerifyFranchiseLogin(LoginModel _input)
        {

            
            string sUrl = LoginUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
           _franchiseCod = _input.UserName;
            // _franchiseCode = (int)System.Web.HttpContext.Current.Session["MyValue"];
           

            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }

            return RedirectToAction("Index", "Franchise", ViewBag.message);

        }

    }
}