﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Diagnostics;
using EllokartAdmin.Models;
using EllokartAdmin.Models.Roles;
using System.Net.Http;
using System.Net.Http.Headers;



namespace EllokartAdmin.Controllers
{
    public class RoleController : Controller
    {
        ForDb db = new ForDb();
        public static string Baseurl = ForDb.BaseURL;
        private string RoleListUrl = Baseurl + "Role/GetAllRoles";
        private string RoleAddUrl = Baseurl + "Role/CreateRole";
        private string RoleEditUrl = Baseurl + "Role/UpdateRoles";
        private string OneRoleUrl = Baseurl + "Role/GetOneRole";
        private string DeleteRoleUrl = Baseurl + "Role/DeleteRole";

        private string RoleAttributeAdd = Baseurl + "Role/CreateRoleAttribute";
        private string RoleAttributeGet = Baseurl + "Role/GetAllRoleAttribute";
        private string DeleteARoleAttribute = Baseurl + "Role/DeleteARoleAttribute";
        private string UpdateRoleAttr = Baseurl + "Role/UpdateRoleAttribute";
        private string OneRoleAttrUrl = Baseurl + "Role/GetOneRoleAttr";
        private string GetAllModuleUrl = Baseurl + "Module/GetAllModule";

        private string GetAllModuleAttributeUrl = Baseurl + "Module/GetAllModuleAttribute";
        private string GetAllRoleAttrUrl = Baseurl + "Role/GetAllRoleAttribute";
        APIResult oResult = null;

        // GET: Role
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Role()
        {

            return View();
               
        }

        public ActionResult RoleList(RoleGetModel _role)
        {

            string sUrl = RoleListUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _role).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();

        }
        public ActionResult CreateRole(Role _role)
        {

            string sUrl = RoleAddUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _role).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Successfully created role ")
            {

            }
            return RedirectToAction("RoleList", "Role");

        }

        public ActionResult EditRole(int Id)
        {

            string sUrl = RoleEditUrl;
            string rUrl = OneRoleUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            RoleGetModel rModel = new RoleGetModel()

            {
                RoleId = Id

            };

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(rUrl, rModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;

            var input = data.sResult;

            Role roleOut = new Role();


            ViewBag.message = input;
            return View();

        }
        public ActionResult RoleUpdate(int RoleId, string RoleName, string Status)
        {
            bool Isactive = false;
         
            if (Status == "on")
            {
                Isactive = true;
            }
            RoleUpdateModel rModel = new RoleUpdateModel()

            {
                roleId = RoleId,
                roleName = RoleName,
                IsActive = Isactive

                // Flag = 1
            };
            string sUrl = RoleEditUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            RoleAttributeModel oModel = new RoleAttributeModel();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, rModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }
         
            return RedirectToAction("RoleList", "Role");

        }

        public ActionResult DeleteRoles(int Id)
        {
            RoleGetModel _input = new RoleGetModel();
            _input.RoleId = Id;



            string sUrl = DeleteRoleUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return RedirectToAction("RoleList", "Role");
        }



        public ActionResult RoleAttributeList(RoleGetModel _roles)
        {
            string sUrl = RoleAttributeGet;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _roles).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();
        }

        public ActionResult RoleAttribute(RoleGetModel _roles)
        {

            //fill dropdown
            string moduleurl = GetAllModuleUrl;
            oResult = new APIResult();
            HttpClient client1 = new HttpClient();
            string token1 = string.Empty;
            client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token1);
            HttpResponseMessage response1 = client1.PostAsJsonAsync(moduleurl, _roles).Result;
            response1.EnsureSuccessStatusCode();
            var data1 = response1.Content.ReadAsAsync<APIResult>().Result;
            var input1 = data1.sResult;
            ViewBag.Mdropdown = input1;


            string moduleAttr = GetAllModuleAttributeUrl;
            oResult = new APIResult();
            HttpClient client2 = new HttpClient();
            string token2 = string.Empty;
            client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token1);
            HttpResponseMessage response2 = client1.PostAsJsonAsync(moduleAttr, _roles).Result;
            response1.EnsureSuccessStatusCode();
            var data2 = response1.Content.ReadAsAsync<APIResult>().Result;
            var input2 = data1.sResult;
            ViewBag.MAdropdown = input1;



            string sUrl = RoleListUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _roles).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();
        }

        public ActionResult CreateRoleAttribute(RoleAttributeModel _role)
        {

            
            string sUrl = RoleAttributeAdd;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            RoleAttributeModel oModel = new RoleAttributeModel();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _role).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Successfully created role attribute ")
            {

            }
            return RedirectToAction("RoleAttributeList", "Role");

        }

        public ActionResult EditRoleAttribute(int Id)
        {

            string sUrl = UpdateRoleAttr;
            string mUrl = OneRoleAttrUrl;
            RoleAttributeModel lmodel = new RoleAttributeModel();
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            RoleAttributeGetModel oModel = new RoleAttributeGetModel()

            {
                roleAttributeId = Id

            };

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(mUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;

            var input = data.sResult;

            RoleAttributeModel roleOut = new RoleAttributeModel();


            ViewBag.message = input;
            return View();

        }

        public ActionResult RoleAttributeUpdate(int attributeId, int roleId, int ModuleId, int moduleAttributeId, string operation, bool status)
        {

          
            RoleAttributeUpdateModel oModel = new RoleAttributeUpdateModel()

            {
                roleAttributeID = attributeId,
                roleId = roleId,
                moduleId = ModuleId,
                moduleAttributeId = moduleAttributeId,
                IsActive = status


            };

            string sUrl = UpdateRoleAttr;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty; 
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }
            
            return RedirectToAction("RoleAttributeList", "Role");

        }
        public ActionResult DeleteRoleAttribute(int Id)
        {
            RoleAttributeGetModel _input = new RoleAttributeGetModel();
            _input.roleAttributeId = Id;



            string sUrl = DeleteARoleAttribute;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return RedirectToAction("RoleAttributeList", "Role");
        }








    }
}