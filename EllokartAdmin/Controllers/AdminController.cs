﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using EllokartAdmin.Models;
using EllokartAdmin.Models.Admin;
using EllokartAdmin.Models.SellerCategory;
using EllokartAdmin.Models.ProductCategory;


namespace EllokartAdmin.Controllers
{
    public class AdminController : Controller
    {


       ForDb db = new ForDb();
        public static string Baseurl = ForDb.BaseURL;
        private string ModuleListUrl = Baseurl + "Module/GetAllModule";
        private string OneModuleUrl = Baseurl + "Module/GetOneModule";
        private string ModuleUrl = Baseurl + "Module/CreateModule";
        private string UpdateModule = Baseurl + "Module/UpdateModule";
        private string ModuleDeletingUrl = Baseurl + "Module/DeleteAModule";


        private string ModuleAttributeListUrl = Baseurl + "Module/GetAllModuleAttribute";
        private string ModuleAttributeUrl = Baseurl + "Module/CreateModuleAttribute";
        private string OneModuleAttrUrl = Baseurl + "Module/GetOneModuleAttr";
        private string UpdateModuleAttr = Baseurl + "Module/UpdateModuleAttribute";
        private string DeleteModuleAttr = Baseurl + "Module/DeleteAmoduleAttribute";

        private string ParentCategGetUrl = Baseurl + "ProductCategory/GetParentCategory";
        private string ProductCategUrL = Baseurl + "ProductCategory/GetAllProductCategory";
        private string ProductCategAddUrL = Baseurl + "ProductCategory/ProductCategory";
        private string ProductCategUpdateUrl = Baseurl + "ProductCategory/UpdateProductCategory";
        private string ProductCategoryDeleteUrl = Baseurl + "ProductCategory/DeleteProductCategory";
        private string ProductCategoryOneUrl = Baseurl + "ProductCategory/GetAProductCategory";

        private string SellerCategListUrl = Baseurl + "SellerCategory/GetAllSellerCategory";
        private string SellerCategAddUrl = Baseurl + "SellerCategory/CreateSellerCategory";
        private string SellerCategUpdateUrl = Baseurl + "SellerCategory/UpdateSellerCategory";
        private string SellerCategDeleteUrl = Baseurl + "SellerCategory/DeleteSellerCategory";
        private string OneSellerCategory = Baseurl + "SellerCategory/GetOneSellerCateg";
        private string BusinessTypeGetUrl = Baseurl + "Business/GetAllBusinessType";

        private string VerifiedBusinesses = Baseurl + "Verification/GetAllVerifiedBusinesses";
        private string DeActivateSeller = Baseurl + "General/AccountDeactivate";


        APIResult oResult = null;




        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Module()
        {
            return View();
        }

        public ActionResult ModuleList(ModuleGetModel _module)
        {

            string sUrl = ModuleListUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _module).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();

        }


        public ActionResult CreateModule(ModuleOutputModel _module)
        {

            string sUrl = ModuleUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            ModuleOutputModel oModel = new ModuleOutputModel();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _module).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Successfully created module")
            {

            }
           return RedirectToAction("ModuleList", "Admin");

        }



        public ActionResult EditModule(int Id)
        {
            string sUrl = UpdateModule;
            string mUrl = OneModuleUrl;
            ModuleOutputModel lmodel = new ModuleOutputModel();
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            ModuleGetModel oModel = new ModuleGetModel()

            {
                moduleId = Id

            };

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(mUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;

            var input = data.sResult;

            ModuleOutputModel moduleOut = new ModuleOutputModel();


            ViewBag.message = input;
            return View();

        }
        public ActionResult DeleteModules(int Id)
        {
            ModuleGetModel _input = new ModuleGetModel();
            _input.moduleId = Id;



            string sUrl = ModuleDeletingUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return RedirectToAction("ModuleList", "Admin");
        }



        public ActionResult ModuleUpdate(int ModuleId, string ModuleName, string Status)
        {
            bool Isactive = false;
           
            if (Status == "on")
            {
                Isactive = true;
            }

            ModuleUpdateModel oModel = new ModuleUpdateModel()

            {
                moduleId = ModuleId,
                moduleName = ModuleName,
                IsActive = Isactive

                // Flag = 1
            };
            string sUrl = UpdateModule;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }
            return RedirectToAction("ModuleList", "Admin");

        }

        public ActionResult ModuleAttributeList(ModuleGetModel _module)



        {

            string sUrl = ModuleAttributeListUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _module).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();

        }


        public ActionResult ModuleAttribute(ModuleGetModel _moduleGet)
        {

            string sUrl = ModuleListUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _moduleGet).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {

                ViewBag.message = data.sResult;

            }
            return View();
        }


        public ActionResult CreateModuleAttribute(int ModuleId, string operation, string status)
        {

         
            ModuleAttributeModel _input = new ModuleAttributeModel()
            {
                moduleId = ModuleId,
                Operation = operation,
                IsActive = true
            };
            string sUrl = ModuleAttributeUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            ModuleAttributeModel oModel = new ModuleAttributeModel();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }

            return RedirectToAction("ModuleAttributeList", "Admin");

        }


        public ActionResult EditModuleAttribute(int Id)
        {
            //fill dropdown
            string modulUrl = ModuleListUrl;
            oResult = new APIResult();
            HttpClient client1 = new HttpClient();
            string token1 = string.Empty;
            client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token1);
            HttpResponseMessage response1 = client1.PostAsJsonAsync(modulUrl, Id).Result;
            response1.EnsureSuccessStatusCode();
            var data1 = response1.Content.ReadAsAsync<APIResult>().Result;
            var input1 = data1.sResult;
            ViewBag.dropdown = input1;


            // fill the fields to edit
            string sUrl = UpdateModuleAttr;
            string mUrl = OneModuleAttrUrl;
            ModuleAttributeModel lmodel = new ModuleAttributeModel();
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            ModuleAttributeGetModel oModel = new ModuleAttributeGetModel()

            {
                AttributeId = Id

            };

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(mUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;

            var input = data.sResult;

            ModuleAttributeModel moduleOut = new ModuleAttributeModel();


            ViewBag.message = input;
            return View();




        }

        public ActionResult ModuleAttributeUpdate(int attributeId, int moduleID, string operation, string status)
        {


            bool Isactive = false;
           
            if (status == "on")
            {
                Isactive = true;
            }
            ModuleAttributeUpdateModel oModel = new ModuleAttributeUpdateModel()

            {
                attributeId = attributeId,
                moduleId = moduleID,
                operation = operation,
                IsActive = Isactive

                // Flag = 1
            };
            string sUrl = UpdateModuleAttr;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }
          
            return RedirectToAction("ModuleAttributeList", "Admin");

        }
        public ActionResult DeleteModuleAttribute(int Id)
        {
            ModuleAttributeGetModel _input = new ModuleAttributeGetModel();
            _input.AttributeId = Id;

            string sUrl = DeleteModuleAttr;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return RedirectToAction("ModuleAttributeList", "Module");
        }
        public ActionResult ProductCategoryList(ProductModel _PdtModel)
        {
            string sUrl = ProductCategUrL;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _PdtModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();

        }


        public ActionResult ProductCategory(ProductModel _PdtModel)
        {
            string sUrl = ParentCategGetUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _PdtModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
              ViewBag.message = data.sResult;

            }
            return View();
        }
        public ActionResult CreateProductCateg(int parentCatId, string productCategory, string description, string status, string createdBy)
        {

          
            ProductCategSave _pdtCategory = new ProductCategSave()
            {
                CategoryName = productCategory,
                ParentCategoryId = parentCatId,
                Description = description,
                IsActive = true,
                CreatedBy = createdBy
            };

            string sUrl = ProductCategAddUrL;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _pdtCategory).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }
            return RedirectToAction("ProductCategoryList", "Admin");


        }


        public ActionResult EditProductCateg(int Id)
        {
            //fill dropdown
            string modulUrl = ParentCategGetUrl;
            oResult = new APIResult();
            HttpClient client1 = new HttpClient();
            string token1 = string.Empty;
            client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token1);
            HttpResponseMessage response1 = client1.PostAsJsonAsync(ParentCategGetUrl, Id).Result;
            response1.EnsureSuccessStatusCode();
            var data1 = response1.Content.ReadAsAsync<APIResult>().Result;
            var input1 = data1.sResult;
            ViewBag.dropdown = input1;



            // fill the fields to edit
            string sUrl = ProductCategUpdateUrl;
            string mUrl = ProductCategoryOneUrl;
            ProductCategSave lmodel = new ProductCategSave();
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            ProductModel pModel = new ProductModel()

            {
                ProductCategoryId = Id

            };

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(mUrl, pModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;

            var input = data.sResult;

            ProductModel pdtCategOut = new ProductModel();


            ViewBag.message = input;
            return View();


        }


        public ActionResult DeleteProductCateg(int Id)
        {
            ProductModel _input = new ProductModel();
            _input.ProductCategoryId = Id;

            string sUrl = ProductCategoryDeleteUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return RedirectToAction("ProductCategoryList", "Admin");
        }

        public ActionResult ProductCategoryUpdate(int ProductCategoryId, string Productcategory, int ParentCateg, string description, string status, string updatedBy)
        {
            bool Isactive = false;
           
            if (status == "on")
            {
                Isactive = true;
            }
            ProductCategUpdate pModel = new ProductCategUpdate()

            {
                productCategoryId = ProductCategoryId,
                CategoryName = Productcategory,
                ParentCategoryID = ParentCateg,
                Description = description,
                IsActive = Isactive,
                UpdatedBy = updatedBy
            };

            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(ProductCategUpdateUrl, pModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }
        
            return RedirectToAction("ProductCategoryList", "Admin");

        }

        public ActionResult SellerCategory(SellerGetModel _seller)
        {
            string sUrl = BusinessTypeGetUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _seller).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();
        }


        public ActionResult SellerCategoryList(SellerGetModel _seller)
        {

            string sUrl = SellerCategListUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _seller).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();

        }


        public ActionResult CreateSellerCateg(string SellerCategory, int BussinessTypeId, string status)
        {

            

            SellerOutputModel _input = new SellerOutputModel()
            {
                SellerCatName = SellerCategory,
                BussinessTypeId = BussinessTypeId,
                IsActive = true
            };
            string sUrl = SellerCategAddUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            // SellerOutputModel oModel = new SellerOutputModel();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }
           

            
            return RedirectToAction("SellerCategoryList", "Admin");

        }

        public ActionResult EditSellerCateg(int Id)
        {

            //fill dropdown
            string businessTypeUrl = BusinessTypeGetUrl;
            oResult = new APIResult();
            HttpClient client1 = new HttpClient();
            string token1 = string.Empty;
            client1.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token1);
            HttpResponseMessage response1 = client1.PostAsJsonAsync(businessTypeUrl, Id).Result;
            response1.EnsureSuccessStatusCode();
            var data1 = response1.Content.ReadAsAsync<APIResult>().Result;
            var input1 = data1.sResult;
            ViewBag.dropdown = input1;



            string sUrl = SellerCategUpdateUrl;
            string mUrl = OneSellerCategory;
            SellerOutputModel lmodel = new SellerOutputModel();
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            SellerGetModel oModel = new SellerGetModel()

            {
                SellerCatId = Id

            };

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(mUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;

            var input = data.sResult;

            SellerOutputModel sellerCategOut = new SellerOutputModel();


            ViewBag.message = input;
            return View();



        }


        public ActionResult SellerCategUpdate(int sellerCatId, string sellerCat, int BusinessType, string Status)
        {
            bool Isactive = false;

           
            if (Status == "on")
            {
                Isactive = true;
            }

            SellerCategUpdateModel oModel = new SellerCategUpdateModel()

            {
                SellerCatId = sellerCatId,
                SellerCatName = sellerCat,
                IsActive = Isactive,
                BussinessTypeId = BusinessType

                // Flag = 1
            };
            string sUrl = SellerCategUpdateUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            SellerOutputModel lmodel = new SellerOutputModel();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            //HttpResponseMessage response = client.GetAsync(sUrl).Result;
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, oModel).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage != "Success")
            {

            }
          
            return RedirectToAction("SellerCategoryList", "Admin");

        }

        public ActionResult DeleteSellerCateg(int Id)
        {
            SellerGetModel _input = new SellerGetModel();
            _input.SellerCatId = Id;



            string sUrl = SellerCategDeleteUrl;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return RedirectToAction("SellerCategoryList", "Admin");
        }


        public ActionResult VerifiedSellersAdmin(Verifiedseller _input)
        {
            string sUrl = VerifiedBusinesses;
            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _input).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {
                ViewBag.message = data.sResult;
            }
            return View();
        }






        public ActionResult DeActivateVerifiedSellers(long Id)
        {
            string sUrl = DeActivateSeller;
            DeActivateSeller _deActivate = new DeActivateSeller();
            _deActivate.UserBusinessId = Id;

            oResult = new APIResult();
            HttpClient client = new HttpClient();
            string token = string.Empty;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            HttpResponseMessage response = client.PostAsJsonAsync(sUrl, _deActivate).Result;
            response.EnsureSuccessStatusCode();
            var data = response.Content.ReadAsAsync<APIResult>().Result;
            if (data.sMessage == "Success")
            {

                ViewBag.message = data.sResult;
            }

            return RedirectToAction("VerifiedSellersAdmin");
        }

    }
}