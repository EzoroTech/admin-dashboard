﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EllokartAdmin.Models.DeActivate
{
    public class DeactivateSellerInput
    {
        public string MobileNo { get; set; }
    }
}