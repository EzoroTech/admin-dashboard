﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EllokartAdmin.Models.SKU
{
    public class SKUinputMdl
    {

        public string  StockKeepingUnit { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; } = DateTime.Now;

    }

    public class GetSku
    {

        public int StockKeepingUnitId { get; set; }
    }
    public class SkuEditMdl
    {

        public int StockKeepingUnitId { get; set; }
        public string StockKeepingUnit { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
    }

}