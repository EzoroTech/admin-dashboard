﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EllokartAdmin.Models.SellerCategory
{
    public class SellerGetModel
    {
        public int SellerCatId { get; set; }
    }

    public class SellerOutputModel
    {
        public string SellerCatName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        public string UpdatedBy { get; set; }
        public int BussinessTypeId { get; set; }

    }
    public class SellerCategUpdateModel
    {
        public int SellerCatId { get; set; }
        public string SellerCatName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public int CreatedBy { get; set; } = 0;
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        public int UpdatedBy { get; set; } = 0;
        public int BussinessTypeId { get; set; }

    }

    public class Verifiedseller
    {
        public long UserId { get; set; }
    }
    public class DeActivateSeller
    {
        public long UserBusinessId { get; set; }
    }
}