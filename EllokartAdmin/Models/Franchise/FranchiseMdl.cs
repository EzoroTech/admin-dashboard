﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EllokartAdmin.Models.Franchise
{
    public class FranchiseMdl
    {
        public string FranchiseCode { get; set; }
        public string PhoneNo { get; set; }
        public string EmailId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public bool IsActive { get; set; }
        public string Password { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public string CreatedBy { get; set; }

    }
    public class FrachiseGetModel
    {
        public int FranchiseId { get; set; } = 0;

    }
    public class FranchiseUpdateModel
    {
        public int FranchiseId { get; set; }
        public string FranchiseCode { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public bool IsActive { get; set; }
        public string Password { get; set; }
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        public int UpdatedBy { get; set; } = 0;
    }
    public class GenerateCode
    {
        public string createdBy { get; set; }
        public int PlanId { get; set; }
    }

    public class FranchiseSeller
    {

        public string FranchiseCode { get; set; }

    }


}