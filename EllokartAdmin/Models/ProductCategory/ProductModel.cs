﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EllokartAdmin.Models.ProductCategory
{
    public class ProductModel
    {
        public int ProductCategoryId { get; set; }

    }
    public class ProductCategSave
    {
        public string CategoryName { get; set; }
        public int ParentCategoryId { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }

    }
    public class ProductCategUpdate
    {
        public int productCategoryId { get; set; }
        public string CategoryName { get; set; }
        public int ParentCategoryID { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string UpdatedBy { get; set; }

    }
    public class ProductBrand
    {

        public string BrandCode { get; set; }
        public int ProductCategoryId { get; set; }
        public string BrandName { get; set; }
        public bool IsActive { get; set; } = true;
        public string ManufactureName { get; set; }
        public string Description { get; set; }

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

    }
    public class ProductbrandbyId
    {
        public int BrandId { get; set; }
    }
    public class updatebrand
    {
        public long BrandId { get; set; }
        public string BrandCode { get; set; }
        public int ProductCategoryId { get; set; }
        public string BrandName { get; set; }
        public bool IsActive { get; set; }
        public string ManufactureName { get; set; }
        public string BrandDescription { get; set; }
        public string UpdatedBy { get; set; }
    }
    public class ProductCategory
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }

    }

    public class SearchByNumber
    {
        public string MobileNo { get; set; }
    }
    public class SearchByDate
    {
        public string CreatedOn { get; set; }
    }

}