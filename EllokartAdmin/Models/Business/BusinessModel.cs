﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EllokartAdmin.Models.Business
{
    public class BusinessModel
    {

        public string bussinessCategoryName { get; set; }
        public string bussinessCategoryDecr { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public int CreatedBy { get; set; } = 0;
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        public int UpdatedBy { get; set; } = 0;
        public bool isActive { get; set; } = true;
        //  public int flag { get; set; }
        public int bussinessTypeId { get; set; }


    }
    public class GetBusinessType
    {
        public int bussinessTypeId { get; set; }
     

    }
    public class BusinessCategUpdateModel
    {
        public int bussinessCategoryId { get; set; }
        public string bussinessCategoryName { get; set; }
        public string bussinessCategoryDecr { get; set; }
        public int CreatedBy { get; set; } = 0;
        public int UpdatedBy { get; set; } = 0;
        public bool isActive { get; set; } = true;
        // public int flag { get; set; }
        public int bussinessTypeId { get; set; }

    }
    public class BusinessTypeModel
    {

        public string bussinessTypeName { get; set; }
        public string bussinessTypeDecr { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public DateTime UpdatedOn { get; set; } = DateTime.Now;

        public int CreatedBy { get; set; } = 0;
        public int UpdatedBy { get; set; } = 0;
      
        public bool isActive { get; set; } = true;
    }

    public class BusinessTypeUpdateModel
    {
        public int bussinessTypeId { get; set; }
        public string bussinessTypeName { get; set; }
        public string bussinessTypeDecr { get; set; }
        public int CreatedBy { get; set; } = 0;
        public int UpdatedBy { get; set; } = 0;
        
        public bool isActive { get; set; } = true;
    }


    public class GetBusinessCategory
    {
        public int bussinessCategoryId { get; set; }
    }


}