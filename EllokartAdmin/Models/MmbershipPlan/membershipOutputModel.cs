﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EllokartAdmin.Models.MmbershipPlan
{
    public class membershipOutputModel
    
    {
            public string MembershipPlan { get; set; }
        public float Price { get; set; }
        public int ValidityInMonths { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int TaxId { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public int CreatedBy { get; set; } = 0;
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        public int UpdatedBy { get; set; } = 0;

    }
    public class membershipGetModel
    {
        public int MembershipPlanId { get; set; } = 0;
    }
    public class membershipUpdateModel
    {

        public int MembershipPlanId { get; set; }
        public string MembershipPlan { get; set; }
        public float Price { get; set; }
        public int ValidityInMonths { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int TaxId { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public int CreatedBy { get; set; } = 0;
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        public int UpdatedBy { get; set; } = 0;
    }




}

