﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EllokartAdmin.Models.Payment
{
    public class PaymentCode
    {
        public string MobileNo { get; set; }
        public long PlanId { get; set; }
        public string MembershipPlan { get; set; }
    }
}