﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EllokartAdmin.Models.Tax
{
    public class TaxInput
    {
        public string taxName { get; set; }
        public string taxPercentage { get; set; }

        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public int CreatedBy { get; set; } = 0;
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        public int UpdatedBy { get; set; } = 0;

    }

    public class GetTaxModel
    {
        public int TaxId { get; set; }

    }
    public class TaxUpdateModel
    {
        public int TaxId { get; set; }
        public string TaxName { get; set; }
        public string taxPercentage { get; set; }
        public string TaxType { get; set; }

        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        public string UpdatedBy { get; set; }

    }
}