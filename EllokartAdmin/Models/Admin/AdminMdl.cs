﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EllokartAdmin.Models.Admin
{
    public class AdminMdl
    {

            public string ModuleName { get; set; }
            public bool IsActive { get; set; }
            public DateTime CreatedOn { get; set; } = DateTime.Now;
            public int CreatedBy { get; set; } = 0;
            public DateTime UpdatedOn { get; set; } = DateTime.Now;
            public int UpdatedBy { get; set; } = 0;
       
    }
    public class ModuleGetModel
    {
        public int moduleId { get; set; } = 0;
        // public int Flag { get; set; } = 0;

    }
    public class ModuleUpdateModel
    {
        public int moduleId { get; set; }
        public string moduleName { get; set; }
        public bool IsActive { get; set; }

    }


    public class ModuleAttributeModel
    {
        public int moduleId { get; set; }
        //  public int attributeid { get; set; }
        public string Operation { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public int CreatedBy { get; set; } = 0;
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        public int UpdatedBy { get; set; } = 0;
        //  public int flag { get; set; }


    }
    public class ModuleAttributeGetModel
    {
        public int AttributeId { get; set; } = 0;
        // public int Flag { get; set; } = 0;

    }

    public class ModuleAttributeUpdateModel
    {
        public int attributeId { get; set; }
        public int moduleId { get; set; }
        public string operation { get; set; }
        public bool IsActive { get; set; }
        //public DateTime UpdatedOn { get; set; } = DateTime.Now;
        //public int UpdatedBy { get; set; } = 0;
    }
    public class ModuleOutputModel
    {

        public string ModuleName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public int CreatedBy { get; set; } = 0;
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        public int UpdatedBy { get; set; } = 0;
    }

}