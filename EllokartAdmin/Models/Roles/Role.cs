﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EllokartAdmin.Models.Roles
{
    public class Role
    {
        public string RoleName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public int CreatedBy { get; set; } = 0;
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        public int UpdatedBy { get; set; } = 0;


    }

    public class RoleGetModel
    {
        public int RoleId { get; set; } = 0;
        // public int Flag { get; set; } = 0;


    }
    public class RoleUpdateModel
    {
        public int roleId { get; set; }
        public string roleName { get; set; }
        public bool IsActive { get; set; }
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        public int UpdatedBy { get; set; } = 0;
    }
    public class RoleAttributeModel
    {

        //public int roleAttributeID { get; set; }
        public string createdBy { get; set; }
        public int updatedBy { get; set; }
        public int flag { get; set; }
        public int roleId { get; set; }
        public int moduleId { get; set; }
        public int moduleAttributeId { get; set; }
        public bool IsActive { get; set; }

    }
    public class RoleAttributeFillModel
    {

        //public int roleAttributeID { get; set; }
        public string createdBy { get; set; }
        public int updatedBy { get; set; }
        public int flag { get; set; }
        public int roleId { get; set; }
        public int moduleId { get; set; }
        public string moduleName { get; set; }
        public int moduleAttributeId { get; set; }
        public string moduleAttribute { get; set; }
        public bool IsActive { get; set; }

    }

    public class RoleAttributeGetModel
    {
        public int roleAttributeId { get; set; }
        //public int Flag { get; set; }


    }
    public class RoleAttributeUpdateModel
    {

        public int roleAttributeID { get; set; }
        public string createdBy { get; set; }
        public int updatedBy { get; set; }
        public int flag { get; set; }
        public int roleId { get; set; }
        public int moduleId { get; set; }
        public int moduleAttributeId { get; set; }
        public bool IsActive { get; set; }

    }
}
