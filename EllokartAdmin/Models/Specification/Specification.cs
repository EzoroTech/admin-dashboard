﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EllokartAdmin.Models.Specification
{
    public class Specification
    {
        public int ProductCategoryId { get; set; }
        public List<PdtCatSpecList> DtProductSpec { get; set; }
    }
    public class PdtCatSpecList
    {
        public long ProductSpecId { get; set; }
    }
    public class ProductCategModel
    {

        public int PdtCategId { get; set; }
    }

    public class ProductSpecModel
    {
        public string specificationName { get; set; }
        public string description { get; set; }
        public bool IsActive { get; set; }

        public string CreatedBy { get; set; }
    }

    public class ProductSpecGetModel
    {

        public int ProductSpecId { get; set; }

    }
    public class ProductSpecUpdateModel
    {
        public int ProductSpecificationId { get; set; }
        public string specificationName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string UpdatedBy { get; set; }

    }
}
