﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EllokartAdmin.Models
{
    public class APIResult
    {
        public int iRet { get; set; }
        public object sResult { get; set; }
        public string sError { get; set; }
        public string sMessage { get; set; }
        public DateTime? CurDate { get; set; }

    }
}