﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EllokartAdmin.Models.CustomerSupport
{
    public class CustomerSuppoerMdl
    {
        public string createdBy { get; set; }
        public int PlanId { get; set; }
    }

    public class BusinessGetModel
    {
        public long UserId { get; set; }
    }
    public class BusinessApprovalModel
    {


        public long UserBusinessId   { get; set; }
        public int IsActive { get; set; }
    
    }
    public class verifiedBusinessUpdate
    {
        public int BranchId { get; set; }
        public string BusinessName { get; set; }
        public string BusinessDisplayName { get; set; }
        public string ContactPerson { get; set; }
        public string LocationName { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Pincode { get; set; }
        public string EmailId { get; set; }
        public string Description { get; set; }
        public string UpdatedBy { get; set; }
    }


    public class BusinessUpgradeModel
    {


        public long UserBusinessId { get; set; }
        public int BusinessStatus { get; set; }
    }

    public class CommentSaveModel
    {
        public long UserId { get; set; }
        public string Comments { get; set; }
    }

}